import React from 'react';

const Icons = () => {
  return (
    <svg className="hidden">
			<symbol id="icon-arrow" viewBox="0 0 24 24">
				<title>arrow</title>
				<polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
			</symbol>
			<symbol id="icon-nav" viewBox="0 0 407 660">
				<title>caret</title>
				<path d="M77 0L0 77l253 253L0 583l77 77 330-330z"/>
			</symbol>
		</svg>
  );
};

export default Icons;