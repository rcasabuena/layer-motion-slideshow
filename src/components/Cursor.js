import React, { Component } from 'react';
import { getMousePos, lerp } from './Utils';

class CursorFx {
  constructor(el) {
      this.DOM = {el: el};
      this.DOM.dot = this.DOM.el.querySelector('.cursor__inner--dot');
      this.DOM.circle = this.DOM.el.querySelector('.cursor__inner--circle');
      this.bounds = {dot: this.DOM.dot.getBoundingClientRect(), circle: this.DOM.circle.getBoundingClientRect()};
      this.scale = 1;
      this.opacity = 1;
      this.mousePos = {x:0, y:0};
      this.lastMousePos = {dot: {x:0, y:0}, circle: {x:0, y:0}};
      this.lastScale = 1;
      this.lastOpacity = 1;
      
      this.initEvents();
      requestAnimationFrame(() => this.render());
  }
  initEvents() {
      window.addEventListener('mousemove', ev => this.mousePos = getMousePos(ev));
  }
  render() {
      this.lastMousePos.dot.x = lerp(this.lastMousePos.dot.x, this.mousePos.x - this.bounds.dot.width/2, 1);
      this.lastMousePos.dot.y = lerp(this.lastMousePos.dot.y, this.mousePos.y - this.bounds.dot.height/2, 1);
      this.lastMousePos.circle.x = lerp(this.lastMousePos.circle.x, this.mousePos.x - this.bounds.circle.width/2, 0.15);
      this.lastMousePos.circle.y = lerp(this.lastMousePos.circle.y, this.mousePos.y - this.bounds.circle.height/2, 0.15);
      this.lastScale = lerp(this.lastScale, this.scale, 0.15);
      this.lastOpacity = lerp(this.lastOpacity, this.opacity, 0.1);
      this.DOM.dot.style.transform = `translateX(${(this.lastMousePos.dot.x)}px) translateY(${this.lastMousePos.dot.y}px)`;
      this.DOM.circle.style.transform = `translateX(${(this.lastMousePos.circle.x)}px) translateY(${this.lastMousePos.circle.y}px) scale(${this.lastScale})`;
      this.DOM.circle.style.opacity = this.lastOpacity
      requestAnimationFrame(() => this.render());
  }
}

class Cursor extends Component {

  constructor(props) {
    super(props);
    this.el = null;
    this.cursor = null;
  };

  componentDidMount() {
    this.cursor = new CursorFx(this.el);
    this.initEvents();
  };

  initEvents() {
    [...document.querySelectorAll('[data-hover]')].forEach((link) => {
      link.addEventListener('mouseenter', () => this.enter() );
      link.addEventListener('mouseleave', () => this.leave() );
      link.addEventListener('click', () => this.click() );
    });
  }

  enter() {
    this.cursor.scale = 2.7;
  }
  leave() {
    this.cursor.scale = 1;
  }
  click() {
    this.cursor.lastScale = 1;
    this.cursor.lastOpacity = 0;
  }

  render() {
    return (
      <div ref={el => this.el = el} className="cursor">
        <div className="cursor__inner cursor__inner--circle"></div>
        <div className="cursor__inner cursor__inner--dot"></div>
      </div>
    );
  };
};

export default Cursor;