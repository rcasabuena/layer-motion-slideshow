import React from 'react';

const Frame = () => {
  return (
    <div className="frame">
      <div className="frame__title-wrap">
        <h1 className="frame__title">Layer Motion Slideshow</h1>
        <div className="nav">
          <div className="nav__counter">
            <span>0</span>/<span>0</span>
          </div>
          <div className="nav__arrows">
            <button className="nav__arrow nav__arrow--prev" data-hover>
              <svg className="icon icon--rotated icon--nav"><use xlinkHref="#icon-nav"></use></svg>
            </button>
            <button className="nav__arrow nav__arrow--next" data-hover>
              <svg className="icon icon--nav"><use xlinkHref="#icon-nav"></use></svg>
            </button>
          </div>
        </div>
      </div>
      <div className="frame__links">
        <a href="https://bitbucket.org/rcasabuena/layer-motion-slideshow" title="Find this project on Bitbucket" data-hover>Bitbucket</a>
      </div>
      <div className="index" data-hover>index</div>
    </div>
  );
};

export default Frame;