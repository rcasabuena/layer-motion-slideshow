import React from 'react';
import './App.css';
import Icons from './components/Icons';
import Frame from './components/Frame';
import Cursor from './components/Cursor';
import SlideShows from './components/SlideShows';

function App() {
  return (
    <>
      <Icons />
      <main>
        <p className="message">Please view on desktop to see the full layout</p>
        <Frame />
        <SlideShows />
      </main>
      <Cursor />
    </>
  );
}

export default App;
